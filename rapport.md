# TP4 Sandbox applicative

## La sandbox applicative et adb

**1.Dans le contexte d'un shell adb, récupérer les informations d'éxécution du processus courant, en particulier:**
- l'identité sous laquelle tourne le processus, ainsi que les groupes secondaires
- le mode du filtre seccomp
- le contexte SELinux

Quel est le sens de la valeur associée à seccomp ?

On commence par ouvrir un shell sur l'émulateur:
`adb shell`

Tout d'abord, on peut obtenir l'identité du processus courant, soit le shell, en réalisant un `echo $$`. 
Pour ma part le pid est 6941.

Ensuite, pour identifier les groupes secondaires du shell on peut utiliser la commande `groups` qui nous retourne les groupes secondaires du processus shell:
`input log adb sdcard_rw sdcard_r net_bt_admin net_bt inet net_bw_stats readproc uhid
`
De manière plus générale nous pouvons obtenir ces informations en dans `/proc/<pid>/status`

Dans ce fichier on trouve une information sur le filtre seccomp, on remarque que la valeur de seccomp est à 0. Cela signifie qu'aucun filtre n'est appliqué.

Enfin, SELinux est activé car lorsque l'on entre `getenforce` la valeur retrournée est Enforcing.

On peut voir le contexte du shell avec la commande `id -Z` qui nous retourne:
`context=u:r:shell:s0
` 
On apprend notamment que du point de vue de SELinux il est de type shell, on suppose donc que la politique SELinux est basé sur son type. De plus, la stratégie MLS (Multi-Layer Security) est s0 soit la plus permissive.

## Implémentation au sein d'une application

**2. Implémenter cette activité d'exécution de commande shell au sein de l'app. Vous afficherez les résultats dans un TextView scrollable, et ajouterez un bouton et un champ texte pour pouvoir indiquer la commande shell, et lancer son exécution.**
En premier lieu,l'activité principale doit permettre de récupérer le contenu d'un EditText (une commande shell) lorsque l'on clique sur le bouton envoyer afin de déclencher un appel à la fonction native en C. Cette fonction permet donc d'éxécuter la commande avec l'identité de l'application. Ensuite le retour est récupéré et affiché dans une TextView.

Voici le MainActivity:
```
package com.andsec.tp3.native_tp4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.andsec.tp3.native_tp4.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native_tp4' library on application startup.
    static {
        System.loadLibrary("native_tp4");
    }

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sendCommand(View view) {
        EditText text = (EditText)findViewById(R.id.editCommand);
        String value = text.getText().toString();
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Example of a call to a native method
        TextView tv = binding.sampleText;
        tv.setMovementMethod(new ScrollingMovementMethod());
        tv.setText(eval(value));
    }
    /**
     * A native method that is implemented by the 'native_tp4' native library,
     * which is packaged with this application.
     */
    private native String eval(String cmd);
}
```
Voici le résultat:
![fc7e91a20d328a177b6a826a31c2971c.png](:/349bd33bd42e49afbf25eabcbaa63694)

Maintenant, il faut ajouter la fonction C qui nous a été donnée:
```
#include <jni.h>
#include <string>
#include <android/log.h>
#include <unistd.h>
#include <setjmp.h>
#include <signal.h>
#include <assert.h>
#include <asm/unistd.h>

static const char *tag = "telescope";

extern "C" JNIEXPORT jstring JNICALL
Java_com_andsec_tp3_native_1tp4_MainActivity_eval(JNIEnv* env, jobject /* this */, jstring cmd) {
    int ret;
    char output_bytes[4096];
    FILE *f;
    const char *cmd_bytes;
    jstring output;

    cmd_bytes = env->GetStringUTFChars(cmd, NULL);
    if (!cmd_bytes) {
        __android_log_print(ANDROID_LOG_DEBUG, tag, "failed to get bytes of cmd string\n");
        goto failed_cmd_bytes;
    }

    f = popen(cmd_bytes, "r");
    if (!f) {
        __android_log_print(ANDROID_LOG_DEBUG, tag, "failed to create process\n");
        goto failed_popen;
    }

    memset(output_bytes, 0, sizeof(output_bytes));
    ret = fread(output_bytes, sizeof(output_bytes)-1, 1, f);
    if (ferror(f)) {
        __android_log_print(ANDROID_LOG_DEBUG, tag, "failed to read command output\n");
        goto failed_fread;
    }

    output_bytes[sizeof(output_bytes)-1] = 0;

    output = env->NewStringUTF(output_bytes);
    if (!output) {
        __android_log_print(ANDROID_LOG_DEBUG, tag, "failed to create output string\n");
        goto failed_fread;
    }

    pclose(f);
    env->ReleaseStringUTFChars(cmd, cmd_bytes);

    return output;

    failed_fread:
    pclose(f);
    failed_popen:
    env->ReleaseStringUTFChars(cmd, cmd_bytes);
    failed_cmd_bytes:
    return env->NewStringUTF("<error>");
}
```

**3. En utilisant votre application, récupérer les informations d’exécution de
l’application, en particulier :**
- **l’identité sous laquelle tourne l’app, ainsi que les groupes secondaires**
- **le mode du filtre seccomp**
- **le contexte SELinux**
**Interpréter les résultats de l’app par rapport à ceux obtenus avec adb en Q1.**

On obtient donc les premières informations en entrant la commande `cat /proc/$$/status` dans la zone de texte de l'application:
- `Pid: 10429`
- `groupe secondaire: 9997 20152 50152`
- `Seccomp: 2`
- `context=u:r:untrusted_app:s0:c152,c256,c512,c768`

Contrairement au shell, le processus d'exécution de l'application est en sandbox car c'est une application non identifié et à laquelle le système n'accorde pas sa confiance.
On peut le remarquer avec le filtre Seccomp à 2, qui signifie qu'on utilise le seccomp-bpf, permettant de filtrer les appels systèmes provenant de l'application.
Mais aussi, le contexte SELinux indique un type `unstrusted_app` et a le privilège d'accéder aux fichiers appartenant aux catégories `c152,c256,c512,c768`.

## Vérification du Discretionary Access Control
**4. Tenter d’accéder aux fichiers d’une autre de vos applications déjà installée sur
l’émulateur. Vous pourrez par exemple lister les propriétés du répertoire de
l’application. Qu’observez vous ? Expliquer ce comportement. Que pouvez-vous
conclure de l’isolation entre applications, sur le répertoire de données de chaque app ?**
En réalisant un `ls -l /data/data/com.andsec.tp3.ds2.eop`, on obtient un refus. Ceci est logique car chaque app a un aid propre et le répertoire de cette dernière est initialisé avec les droits 600. Soit des permissions en rw uniquement pour l'app. Ainsi les applications ne peuvent lire les données stockées sous /data/data hormis les leurs.


Ajouter la permission Internet à votre application.
**5. Récupérer les groupes associés au processus de votre application, en utilisant
l’éxécution de commandes de votre app. Retrouvez les significations de ces différents groupes en vous aidant d’Internet et du code source de bionic.**

Les groupes listés sont:
- inet (autorise la création de socket TCP/IP et donc l'accès à internet)
- everybody (les processus appartenant à ce groupe ne pourront ni lire ni écrire dans `/sdcard`, à moins qu'ils disposent des permissions `READ/WRITE_EXTERNAL_STORAGE`)
- u0_a152_cache et all_a152 (groupes générés pour l'application)

## Vérification du Mandatory Access Control SELinux
A l’aide de l’exécution de commande de votre application, retrouvez le répertoire courant de votre app.
 On retrouve le répertoire courant avec `echo $PWD` et nous nous situons à la racine du système.
 **6. Faire le listing du répertoire courant. Pour quelle raison la commande échoue-t-elle ? Identifier les éléments de preuve sur l’émulateur.**
 On entre `ls -l $PWD` et en résulte ceci:
```
ls: type=1400 audit(0.0:1188): avc: denied { read } for name="/" dev="dm-2" ino=2 
scontext=u:r:untrusted_app:s0:c152,c256,c512,c768 tcontext=u:object_r:rootfs:s0 tclass=dir 
permissive=0
```
Cela nous indique que l'objet de type `untrusted_app` n'a pas les permissions SELinux en lecture sur l'objet de type `rootfs`.

A l’aide de la commande run-as , copiez system/bin/sh dans le répertoire de données de
l’application /data/data/<package>.

**7. Qui est le propriétaire de ce nouveau fichier et quels sont les droits associés à ce
fichier ? Quel est le contexte SELinux associé au fichier ?**

Tout d'abord on entre la commande suivante afin de copier le shell sh:
`run-as com.andsec.tp3.native_tp4 --user u0_a152 cp /system/bin/sh /data/data/com.andsec.tp3.native_tp4`
Ensuite on peut lister les droits de ce fichier:
`run-as com.andsec.tp3.native_tp4 --user u0_a152 ls -l /data/data/com.andsec.tp3.native_tp4/sh`

On remarquera que c'est l'utilisateur et le groupe u0_a152 qui est propriétaire de l'exécutable et que seul l'utilisateur propriétaire à des permissions rwx dessus.
Le contexte SELinux est le suivant:
`u:object_r:app_data_file:s0:c152,c256,c512,c768 /data/data/com.andsec.tp3.native_tp4/sh`

Lancer le nouveau shell copié dans le répertoire de l’application avec votre application, à
l’aide de votre fonctionnalité d’exécution de code. Vous pourrez par exemple lancer le shell avec l’option -v qui active le mode verbeux, et vous donnera entre autres la version de ce dernier.

**8. Quelle est la version du shell ? Est-ce que l’exécution dans le contexte de
l’application a fonctionné ? Identifier les éléments de preuve sur l’émulateur.**

Lorsque l'on tente d'exécuter sh depuis l'application, nous voyons ces informations apparaître sur logcat: 
```
sh      : type=1400 audit(0.0:1232): avc: granted { execute } for name="sh" dev="vdc" ino=131301 scontext=u:r:untrusted_app:s0:c152,c256,c512,c768 tcontext=u:object_r:app_data_file:s0:c152,c256,c512,c768 tclass=file
sh      : type=1400 audit(0.0:1234): avc: denied { execute_no_trans } for path="/data/data/com.andsec.tp3.native_tp4/sh" dev="vdc" ino=131301 scontext=u:r:untrusted_app:s0:c152,c256,c512,c768 tcontext=u:object_r:app_data_file:s0:c152,c256,c512,c768 tclass=file permissive=0
```

Cela signifie qu'en premier lieu, l'application est autorisé à exécuter sh avec transition de domaine, mais ensuite l'accès est refusé car notre application tente de l'exécuter sans avoir la permission de réaliser une transition de domaine.
La transition de domaine correspond à faire passer un processus d'un domaine à un autre.

## Vérification du filtrage d’appels système seccomp
Nous allons terminer le TP par l’exploration du seccomp.
L’exécution d’un syscall arbitraire se fera au moyen d’un appel à syscall, présent dans
unistd.h. Vous devez développer une fonction native ayant la signature Java suivante :
int syscall(int syscall_no);
Vous pourrez vous appuyer sur la fonction syscall présente dans la bibliothèque bionic.

**9. A l’aide des informations présentes sur le billet https://android-developers.googleblog.com/2017/07/seccomp-filter-in-android-o.html, sélectionner un appel système interdit et appeler le. Faites attention au numéro d’appel système, qui dépend du bitness du process. Une fois l’appel fait que se passe-t-il ?**

Pour réaliser un appel système interdit, il suffit de créer une fonction native comme ceci:
```
#include <jni.h>
#include <string>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <signal.h>

extern "C"
JNIEXPORT void JNICALL
Java_com_tp4_native_1syscall_MainActivity_syscall(JNIEnv *env, jobject thiz, jint syscall_no) {
    syscall(syscall_no);
}
```

Ensuite, on peut reprendre la méthode de l'exercice précédent en envoyant le numéro d'appel système via un EditText dans l'application. Lors d'un clic sur le bouton cela déclenchera l'appel système. Voici le MainActivity permettant de faire cela:
```
package com.tp4.native_syscall;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.tp4.native_syscall.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native_syscall' library on application startup.
    static {
        System.loadLibrary("native_syscall");
    }

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * A native method that is implemented by the 'native_syscall' native library,
     * which is packaged with this application.
     */
    public void sendSyscall(View view) {
        EditText text = (EditText)findViewById(R.id.editCommand);
        String value = text.getText().toString();
        syscall(Integer.parseInt(value));
    }

    private native void syscall(int syscall_no);
}
```

Après avoir testé cette méthode avec un appel système interdit, par exemple `getppid()`, soit le numéro 64, on observe que l'appli crash avec ceci dans les logs:
` A/libc: Fatal signal 31 (SIGSYS), code 1 (SYS_SECCOMP) in tid 9880 (.native_syscall), pid 9880 (.native_syscall)`
Cela signifie que seccomp a bloqué cet appel système.

Le problème précédent est gênant pour votre application. Il est nécessaire que vous gériez correctement la réception de cet élément sans causer un arrêt de l’application. Vous aurez besoin d’utiliser deux nouvelles fonctions :
**sigaction**, qui permet de manipuler un gestionnaire de signaux ;
**longjmp et setjmp**, qui permettent de sauvegarder un contexte d’exécution et de le restaurer.

**10. A l’aide de ces deux fonctions, rendre l’application fonctionnelle, capable de
déclencher un appel système interdit et de remonter si l’appel est autorisé par seccomp ou non, sans crasher.**
